try
{
	/*
	var TestSingleLocationFlag 			= true;
	var CreateAllPermutationsFlag 		= true;

	var DDRFlag 						= false;
	var AsystFlag 						= true;

	var PCRPlateFlag 					= true;
	var PurificationPlateFlag 			= false;
	var SampleRackFlag 					= false;
	var WastePlateFlag 					= false;
	var TipBoxFlag 						= false;
	*/

	var LabwareLocationsFolder 			= "C:\\lab-automation-support\\cycler-protocols\\Labware Locations\\";
	var ScriptsFolder 					= "C:\\lab-automation-support\\cycler-protocols\\Scripts\\";

	var LocationsFile 					= "Locations.json";

	open(ScriptsFolder + "CyclerCommon.js");

	var Locations 						= LoadLocations(LocationsFile);	
	var SingleLocation 					= null;
}

catch (e)
{
	print("Exception at CyclerInit.js: " + e);
	task.pause();
}


