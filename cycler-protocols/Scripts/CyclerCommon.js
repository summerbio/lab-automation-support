/**
 * Reads file contents 
 *
 * @param {string} filePath - the file path (absolute) to be read
 * @return {string} contents of file
 */
function ReadFile(filePath)
{
	try 
	{
		var jsonFile = new File();
		jsonFile.Open(filePath);
		var jsonFileContents = jsonFile.Read();
		jsonFile.Close();
		return jsonFileContents;
	}
	catch (e)
	{
		print("Exception at ReadFile: " + e);
		task.pause();
	}
}

/**
 * Checks if item is in array
 *
 * @param {[object]} array - The array to be searched through
 * @param {object} item - The item to be search for
 * @return {boolean} true if item is in array, otherwise false
 */
function Includes(array, item)
{
	if (array == null)
	{
		throw "array is null";
	}

	if (item == null)
	{
		throw "item is null";
	}

	for (var i=0; i <array.length; i++)
	{
		if (array[i] == item)
		{
			return true;
		}
	}

	return false;
}

/**
 * Creates a new array with all permutations of the initial array
 *
 * @param {[object]} array - for example: [A,B,C]
 * @return {[object]} for example: [A,B,A,C,A,B,C,B]
 */
 function CreateAllPermutations(initialArray)
{
	try 
	{
		var finalArray = [];
		for (var i=0; i<initialArray.length-1; i++)
		{
			for (var j=i+1; j<initialArray.length; j++)
			{
				finalArray.push(initialArray[i]);
				finalArray.push(initialArray[j]);
			}
			finalArray.push(initialArray[i]);
		}
		return finalArray;
	}
	catch (e)
	{
		print("Exception at CreateAllPermutations: " + e);
		task.pause();
	}
}

/**
 * Creates a new array with singleLocation interleaved with each element in initialArray 
 *
 * @param {[object]} array - for example: [A,B,C]
 * @param {object} singleLocation - for example: X
 * @return {[object]} for example: [X,A,X,B,X,C]
 */
 function CreateSingleLocationTest(initialArray, singleLocation)
{
	try 
	{
		var finalArray = [];
		for (var i=0; i<initialArray.length-1; i++)
		{
			if (!(initialArray[i].Device == singleLocation.Device && initialArray[i].Location == singleLocation.Location))
			{
				finalArray.push(singleLocation);
				finalArray.push(initialArray[i]);
			}
		}
		return finalArray;
	}
	catch (e)
	{
		print("Exception at CreateSingleLocationTest: " + e);
		task.pause;
	}
}

/**
 * Returns filtered locations specific to a labware type and biocel side (includes special treatment for plate exchangers)
 *
 * @param {[object]} locations - All locations on the biocel
 * @param {string} labware - The labware ("PCR", "Purification", "Rack", "Tip", "Waste")
 * @param {string} side - The biocel side ("Asyst", "DDR")
 * @return {[object]} All locations on the biocel filtered according to labware and side
 */
function FilterLocations(locations, labware, side)
{
	try 
	{
		var sideID;

		if (side == "Asyst")
		{
			sideID = '2';
		}
		else if (side == "DDR")
		{
			sideID = '1';
		}
		else
		{
			throw "side is not Asyst or DDR!"
		}

		var filteredLocations = [];
		for (var i=0; i<locations.length; i++)
		{
			var location = locations[i];

			// special logic to handle plate exchangers
			if (location.Device == "1ExchangerLower" || location.Device == "1ExchangerUpper")
			{
				if (AsystFlag && DDRFlag) 
				{
					// don't add exchangers if both Asyst and DDR side are being cycled
				}
				else if (AsystFlag)
				{
					if (location.Locations[0] == "Platepad 1")
					{
						if (location.Enable)
						{
							filteredLocations.push(location)
						}
					}
				}
				else if (DDRFlag)
				{
					if (location.Locations[0] == "Platepad 2")
					{
						if (location.Enable)
						{
							filteredLocations.push(location)
						}					
					}
				}
				else
				{
				}
			}
			// all devices besides plate exchangers 
			else if (Includes(location.Labware, labware) && location.Device[0] == sideID)
			{
				if (location.Enable) 
				{
					filteredLocations.push(location)
				}
			}
			else
			{
			}
		}
		return filteredLocations
	}
	catch (e)
	{
		print("Exception at FilterLocations: " + e);
		task.pause();
	}
}

/**
 * Returns the work order queue for the place plate loop
 *
 * @param {[object]} locations - All filtered locations (specific to a labware and biocel side)
 * @return {[object]} Array popeulated with objects, where each has a "Device" and "Location" field
 */
function BuildLocationArray(locations)
{
	try 
	{
		var finalLocations = [];
		for (var i=0; i<locations.length; i++)
		{
			for (var j=0; j< locations[i].Locations.length; j++)
			{
				finalLocations.push({"Device": locations[i].Device, "Location": locations[i].Locations[j]});
			}
		}

		// TestSingleLocationFlag takes precedence over CreateAllPermuationsFlag
		if (TestSingleLocationFlag)
		{
			return(CreateSingleLocationTest(finalLocations, SingleLocation));
		}
		else if (CreateAllPermutationsFlag)
		{
			return(CreateAllPermutations(finalLocations));
		}
		else 
		{
			return finalLocations
		}
	}
	catch (e)
	{
		print("Exception at BuildLocationArray: " + e);
		task.pause();
	}
}

/**
 * Returns work order queue for the place plate loop by calling helper functions
 *
 * @param {[object]} locations - All locations on the biocel
 * @param {string} labware - The labware ("PCR", "Purification", "Rack", "Tip", "Waste")
 * @param {string} side - The biocel side ("Asyst", "DDR")
 * @return {[object]} All locations on the biocel filtered according to labware and side
 */
function GetLocations(locations, labware, side)
{
	try 
	{
		return BuildLocationArray(FilterLocations(locations, labware, side));
	}
	catch (e)
	{
		print("Exception at GetLocations: " + e);
		task.pause();
	}
}

/**
 * Returns all JSON locations, which will later be filtered according to labware, biocel-side, etc.
 *
 * @param {string} fileName - absolute path to JSON file containing location data
 * @return {[object]} All locations on the biocel
 */
function LoadLocations(fileName) 
{
	try 
	{
		var jsonString = ReadFile(LabwareLocationsFolder + fileName);
		var tempLocations;
		eval("tempLocations = " + jsonString);
		return tempLocations; 

	}
	catch (e)
	{
		print("Exception at LoadLocations: " + e);
		task.pause();
	}
}

/**
 * VWorks syntax to set device/location fields of "Place Plate" task
 *
 * @param {string} device 
 * @param {string} location 
 */
function PlacePlate(device, location)
{
	task.Devicetouse = device;
	task.Locationtouse = location;

	if (task.Devicetouse != device) 
	{
		throw "Invalid Devicetouse: " + device;
	}

	else if (task.Locationtouse != location) 
	{
		throw "Invalid Locationtouse: " + location + " for Devicetouse: " + device;
	}

	else 
	{
		print("Place plate at Device: " + device + ", Location: " + location);
	}
}

/**
 * VWorks syntax to set number of times to loop according to length of work order queue
 *
 * @param {[object]} plateLocations 
 */
function LoopStart(plateLocations)
{
	try 
	{
		task.Numberoftimestoloop = plateLocations.length;
	}
	catch (e)
	{
		print("Exception at LoopStart: " + e);
		task.pause();
	}
}


/**
 * Calls PlacePlate helper function to select location from plateLocations depending on loopCounter
 *
 * @param {[object]} plateLocations 
 * @param {number} loopCounter 
 */
function PlacePlateLoop(plateLocations, loopCounter)
{
	try 
	{
		var device = plateLocations[loopCounter].Device;
		var location = plateLocations[loopCounter].Location;
	
		PlacePlate(device, location);
	}
	catch (e)
	{
		print("Exception at PlacePlateLoop: " + e);
		task.pause();
	}
}

/**
 * Verifies that all locations (where Enable=true) in the JSON file are accessible in VWorks
 *
 * @param {[object]} locations 
 */
function VerifyAllLocations(locations)
{
	print("Begin location verification")
	print("---------------------------")
	loop1:	
	for (var i=0; i< locations.length; i++) 
	{		
		if (locations[i].Enable)
		{
			loop2:
			for (var j=0; j<locations[i].Locations.length; j++)
			{
				try
				{
					PlacePlate(locations[i].Device, locations[i].Locations[j]);
				}
				catch (e)
				{
					print("Exception at VerifyAllLocations: " + e);
					task.pause();
					break loop1;
				}
			}
		}
	}
	task.skip();
	print("---------------------------")
	print("End location verification")
}

/**
 * gather data from VWorks task about device/location selected by user 
 */
function RegisterSingleLocation()
{
	try 
	{
		SingleLocation = {"Device": task.Devicetouse, "Location": task.Locationtouse};

		// skip task so that VWorks doesn't think there is actually a plate at this location
		task.skip();
	}
	catch (e)
	{
		print("Exception at RegisterSingleLocation: " + e)
	}
}

function PCRPlateAsystSpawnGate()
{
	try 
	{
		if (!(AsystFlag && PCRPlateFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at PCRPlateSpawnGate: " + e);
		task.pause();
	}
}

function PCRPlateDDRSpawnGate()
{
	try 
	{
		if (!(DDRFlag && PCRPlateFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at PCRPlateSpawnGate: " + e);
		task.pause();
	}
}

function PurificationPlateAsystSpawnGate()
{
	try 
	{
		if (!(AsystFlag && PurificationPlateFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at PurificationPlateAsystSpawnGate: " + e);
		task.pause();
	}
}

function PurificationPlateDDRSpawnGate()
{
	try 
	{
		if (!(DDRFlag && PurificationPlateFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at PurificationPlateDDRSpawnGate: " + e);
		task.pause();
	}
}

function WastePlateAsystSpawnGate()
{
	try 
	{
		if (!(AsystFlag && WastePlateFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at WastePlateAsystSpawnGate: " + e);
		task.pause();
	}
}

function WastePlateDDRSpawnGate()
{
	try 
	{
		if (!(DDRFlag && WastePlateFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at WastePlateDDRSpawnGate: " + e);
		task.pause();
	}
}

function TipBoxAsystSpawnGate()
{
	try 
	{
		if (!(AsystFlag && TipBoxFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at TipBoxAsystSpawnGate: " + e);
		task.pause();
	}
}

function TipBoxDDRSpawnGate()
{
	try 
	{
		if (!(DDRFlag && TipBoxFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at TipBoxDDRSpawnGate: " + e);
		task.pause();
	}
}

function SampleRackAsystSpawnGate()
{
	try 
	{
		if (!(AsystFlag && SampleRackFlag)) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at SampleRackAsystSpawnGate: " + e);
		task.pause();
	}
}

function SingleLocationSpawnGate()
{
	try 
	{
		if (!TestSingleLocationFlag) 
		{
			task.skip();
		}
	}
	catch (e)
	{
		print("Exception at SingleLocationSpawnGate: " + e);
		task.pause();
	}
}