<?xml version='1.0' encoding='ASCII' ?>
<Velocity11 file='Protocol_Data' md5sum='7a2e3735e8b5cbf56df0f28a7927f3fa' version='2.0' >
	<File_Info AllowSimultaneousRun='1' AutoExportGanttChart='0' AutoLoadRacks='When the main protocol starts' AutoUnloadRacks='0' AutomaticallyLoadFormFile='1' Barcodes_Directory='' ClearInventory='0' DeleteHitpickFiles='1' Description='' Device_File='C:\sb-biocel-settings\Device Files\SB-Production.dev' Display_User_Task_Descriptions='1' DynamicAssignPlateStorageLoad='0' FinishScript='' Form_File='' HandlePlatesInInstance='1' ImportInventory='0' InventoryFile='' Notes='' PipettePlatesInInstanceOrder='0' Protocol_Alias='' StartScript='' Use_Global_JS_Context='0' />
	<Processes >
		<Startup_Processes >
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV1_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:6:2' />
						<Parameter Name='Pin name saved' Value='CONV1_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV2_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:8:2' />
						<Parameter Name='Pin name saved' Value='CONV2_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV3_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:10:2' />
						<Parameter Name='Pin name saved' Value='CONV3_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV4_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:12:2' />
						<Parameter Name='Pin name saved' Value='CONV4_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV5_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:14:2' />
						<Parameter Name='Pin name saved' Value='CONV5_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV6_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:16:2' />
						<Parameter Name='Pin name saved' Value='CONV6_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='turn OFF conveyors' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::JavaScript' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='var Conv1Flag = true;
var Conv2Flag = true;
var Conv3Flag = true;
var Conv4Flag = true;
var Conv5Flag = true;
var Conv6Flag = true;' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='set flags' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='set flags' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
		</Startup_Processes>
		<Main_Processes >
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Spawn Process' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='if (!Conv1Flag)
{
    task.skip();
}' />
					<Parameters >
						<Parameter Category='' Name='Process to spawn' Value='CONV1: Sample Rack' />
						<Parameter Category='' Name='Spawn as subroutine' Value='' />
						<Parameter Category='' Name='Spawn parameter' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Spawn Process' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='if (!Conv2Flag)
{
    task.skip();
}' />
					<Parameters >
						<Parameter Category='' Name='Process to spawn' Value='CONV2: Waste Plate' />
						<Parameter Category='' Name='Spawn as subroutine' Value='' />
						<Parameter Category='' Name='Spawn parameter' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Spawn Process' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='if (!Conv3Flag)
{
    task.skip();
}' />
					<Parameters >
						<Parameter Category='' Name='Process to spawn' Value='CONV3: Tip Box' />
						<Parameter Category='' Name='Spawn as subroutine' Value='' />
						<Parameter Category='' Name='Spawn parameter' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Spawn Process' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='if (!Conv4Flag)
{
    task.skip();
}' />
					<Parameters >
						<Parameter Category='' Name='Process to spawn' Value='CONV4: Purification Plate' />
						<Parameter Category='' Name='Spawn as subroutine' Value='' />
						<Parameter Category='' Name='Spawn parameter' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Spawn Process' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='if (!Conv5Flag)
{
    task.skip();
}' />
					<Parameters >
						<Parameter Category='' Name='Process to spawn' Value='CONV5: Purification Plate' />
						<Parameter Category='' Name='Spawn as subroutine' Value='' />
						<Parameter Category='' Name='Spawn parameter' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Spawn Process' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='if (!Conv6Flag)
{
    task.skip();
}' />
					<Parameters >
						<Parameter Category='' Name='Process to spawn' Value='CONV6: Sample Rack' />
						<Parameter Category='' Name='Spawn as subroutine' Value='' />
						<Parameter Category='' Name='Spawn parameter' Value='' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='scheduler' />
					<Parameter Name='Plate type' Value='' />
					<Parameter Name='Simultaneous plates' Value='1' />
					<Parameter Name='Plates have lids' Value='0' />
					<Parameter Name='Plates enter the system sealed' Value='0' />
					<Parameter Name='Use single instance of plate' Value='1' />
					<Parameter Name='Automatically update labware' Value='0' />
					<Parameter Name='Enable timed release' Value='0' />
					<Parameter Name='Release time' Value='30' />
					<Parameter Name='Auto managed counterweight' Value='0' />
					<Parameter Name='Barcode filename' Value='No Selection' />
					<Parameter Name='Has header' Value='' />
					<Parameter Name='Barcode or header South' Value='No Selection' />
					<Parameter Name='Barcode or header West' Value='No Selection' />
					<Parameter Name='Barcode or header North' Value='No Selection' />
					<Parameter Name='Barcode or header East' Value='No Selection' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV1_DIR' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:7:2' />
						<Parameter Name='Pin name saved' Value='CONV1_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV1_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:6:2' />
						<Parameter Name='Pin name saved' Value='CONV1_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='4' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV1_INSNS' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:1:0' />
						<Parameter Name='Pin name saved' Value='CONV1_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Downstack' >
					<Devices >
						<Device Device_Name='2Conveyor-1' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Downstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Free empty stackers' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Place Plate' >
					<Devices >
						<Device Device_Name='2Hotel-1' Location_Name='Slot 1' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Place Plate' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Device to use' Value='2Hotel-1' />
						<Parameter Category='' Name='Location to use' Value='Slot 1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV1_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:6:2' />
						<Parameter Name='Pin name saved' Value='CONV1_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV1_DIR' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:7:2' />
						<Parameter Name='Pin name saved' Value='CONV1_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='7' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV1_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:6:2' />
						<Parameter Name='Pin name saved' Value='CONV1_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='8' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV1_INSNS' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:1:0' />
						<Parameter Name='Pin name saved' Value='CONV1_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='9' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Upstack' >
					<Devices >
						<Device Device_Name='2Conveyor-1' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='10' />
						<Parameter Category='Task Description' Name='Task description' Value='Upstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='CONV1: Sample Rack' />
					<Parameter Name='Plate type' Value='96 1.4ml Micronic Tube Rack External Thread WITH Caps' />
					<Parameter Name='Simultaneous plates' Value='1' />
					<Parameter Name='Plates have lids' Value='0' />
					<Parameter Name='Plates enter the system sealed' Value='0' />
					<Parameter Name='Use single instance of plate' Value='0' />
					<Parameter Name='Automatically update labware' Value='0' />
					<Parameter Name='Enable timed release' Value='0' />
					<Parameter Name='Release time' Value='30' />
					<Parameter Name='Auto managed counterweight' Value='0' />
					<Parameter Name='Barcode filename' Value='No Selection' />
					<Parameter Name='Has header' Value='' />
					<Parameter Name='Barcode or header South' Value='No Selection' />
					<Parameter Name='Barcode or header West' Value='No Selection' />
					<Parameter Name='Barcode or header North' Value='No Selection' />
					<Parameter Name='Barcode or header East' Value='No Selection' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV2_DIR' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:9:2' />
						<Parameter Name='Pin name saved' Value='CONV2_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV2_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:8:2' />
						<Parameter Name='Pin name saved' Value='CONV2_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='7' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV2_INSNS' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:3:0' />
						<Parameter Name='Pin name saved' Value='CONV2_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Downstack' >
					<Devices >
						<Device Device_Name='2Conveyor-2' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Downstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Free empty stackers' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Place Plate' >
					<Devices >
						<Device Device_Name='2Hotel-1' Location_Name='Slot 2' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Place Plate' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Device to use' Value='2Hotel-1' />
						<Parameter Category='' Name='Location to use' Value='Slot 2' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV2_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:8:2' />
						<Parameter Name='Pin name saved' Value='CONV2_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV2_DIR' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:9:2' />
						<Parameter Name='Pin name saved' Value='CONV2_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='7' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV2_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:8:2' />
						<Parameter Name='Pin name saved' Value='CONV2_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='8' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV2_INSNS' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:3:0' />
						<Parameter Name='Pin name saved' Value='CONV2_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='9' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Upstack' >
					<Devices >
						<Device Device_Name='2Conveyor-2' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='10' />
						<Parameter Category='Task Description' Name='Task description' Value='Upstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='CONV2: Waste Plate' />
					<Parameter Name='Plate type' Value='96 Waste' />
					<Parameter Name='Simultaneous plates' Value='1' />
					<Parameter Name='Plates have lids' Value='0' />
					<Parameter Name='Plates enter the system sealed' Value='0' />
					<Parameter Name='Use single instance of plate' Value='0' />
					<Parameter Name='Automatically update labware' Value='0' />
					<Parameter Name='Enable timed release' Value='0' />
					<Parameter Name='Release time' Value='30' />
					<Parameter Name='Auto managed counterweight' Value='0' />
					<Parameter Name='Barcode filename' Value='No Selection' />
					<Parameter Name='Has header' Value='' />
					<Parameter Name='Barcode or header South' Value='No Selection' />
					<Parameter Name='Barcode or header West' Value='No Selection' />
					<Parameter Name='Barcode or header North' Value='No Selection' />
					<Parameter Name='Barcode or header East' Value='No Selection' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV3_DIR' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:11:2' />
						<Parameter Name='Pin name saved' Value='CONV3_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV3_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:10:2' />
						<Parameter Name='Pin name saved' Value='CONV3_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='6' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV3_INSNS' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:5:0' />
						<Parameter Name='Pin name saved' Value='CONV3_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Downstack' >
					<Devices >
						<Device Device_Name='2Conveyor-3' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Downstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Free empty stackers' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Place Plate' >
					<Devices >
						<Device Device_Name='2Hotel-1' Location_Name='Slot 3' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Place Plate' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Device to use' Value='2Hotel-1' />
						<Parameter Category='' Name='Location to use' Value='Slot 3' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV3_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:10:2' />
						<Parameter Name='Pin name saved' Value='CONV3_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV3_DIR' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:11:2' />
						<Parameter Name='Pin name saved' Value='CONV3_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='7' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV3_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:10:2' />
						<Parameter Name='Pin name saved' Value='CONV3_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='8' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV3_INSNS' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:5:0' />
						<Parameter Name='Pin name saved' Value='CONV3_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='9' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Upstack' >
					<Devices >
						<Device Device_Name='2Conveyor-3' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='10' />
						<Parameter Category='Task Description' Name='Task description' Value='Upstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='CONV3: Tip Box' />
					<Parameter Name='Plate type' Value='96 180ul Filtered Delidded' />
					<Parameter Name='Simultaneous plates' Value='1' />
					<Parameter Name='Plates have lids' Value='0' />
					<Parameter Name='Plates enter the system sealed' Value='0' />
					<Parameter Name='Use single instance of plate' Value='0' />
					<Parameter Name='Automatically update labware' Value='0' />
					<Parameter Name='Enable timed release' Value='0' />
					<Parameter Name='Release time' Value='30' />
					<Parameter Name='Auto managed counterweight' Value='0' />
					<Parameter Name='Barcode filename' Value='No Selection' />
					<Parameter Name='Has header' Value='' />
					<Parameter Name='Barcode or header South' Value='No Selection' />
					<Parameter Name='Barcode or header West' Value='No Selection' />
					<Parameter Name='Barcode or header North' Value='No Selection' />
					<Parameter Name='Barcode or header East' Value='No Selection' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV4_DIR' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:13:2' />
						<Parameter Name='Pin name saved' Value='CONV4_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV4_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:12:2' />
						<Parameter Name='Pin name saved' Value='CONV4_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='6' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV4_INSNS' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:7:0' />
						<Parameter Name='Pin name saved' Value='CONV4_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Downstack' >
					<Devices >
						<Device Device_Name='2Conveyor-4_Purification' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Downstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Free empty stackers' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Place Plate' >
					<Devices >
						<Device Device_Name='2Hotel-1' Location_Name='Slot 4' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Place Plate' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Device to use' Value='2Hotel-1' />
						<Parameter Category='' Name='Location to use' Value='Slot 4' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV4_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:12:2' />
						<Parameter Name='Pin name saved' Value='CONV4_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV4_DIR' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:13:2' />
						<Parameter Name='Pin name saved' Value='CONV4_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='7' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV4_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:12:2' />
						<Parameter Name='Pin name saved' Value='CONV4_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='8' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV4_INSNS' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:7:0' />
						<Parameter Name='Pin name saved' Value='CONV4_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='9' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Upstack' >
					<Devices >
						<Device Device_Name='2Conveyor-4_Purification' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='10' />
						<Parameter Category='Task Description' Name='Task description' Value='Upstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='CONV4: Purification Plate' />
					<Parameter Name='Plate type' Value='96 Purification' />
					<Parameter Name='Simultaneous plates' Value='1' />
					<Parameter Name='Plates have lids' Value='0' />
					<Parameter Name='Plates enter the system sealed' Value='0' />
					<Parameter Name='Use single instance of plate' Value='0' />
					<Parameter Name='Automatically update labware' Value='0' />
					<Parameter Name='Enable timed release' Value='0' />
					<Parameter Name='Release time' Value='30' />
					<Parameter Name='Auto managed counterweight' Value='0' />
					<Parameter Name='Barcode filename' Value='No Selection' />
					<Parameter Name='Has header' Value='' />
					<Parameter Name='Barcode or header South' Value='No Selection' />
					<Parameter Name='Barcode or header West' Value='No Selection' />
					<Parameter Name='Barcode or header North' Value='No Selection' />
					<Parameter Name='Barcode or header East' Value='No Selection' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV5_DIR' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:15:2' />
						<Parameter Name='Pin name saved' Value='CONV5_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV5_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:14:2' />
						<Parameter Name='Pin name saved' Value='CONV5_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='9' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV5_INSNS' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:9:0' />
						<Parameter Name='Pin name saved' Value='CONV5_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Downstack' >
					<Devices >
						<Device Device_Name='2Conveyor-5' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Downstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Free empty stackers' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Place Plate' >
					<Devices >
						<Device Device_Name='2Hotel-1' Location_Name='Slot 5' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Place Plate' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Device to use' Value='2Hotel-1' />
						<Parameter Category='' Name='Location to use' Value='Slot 5' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV5_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:14:2' />
						<Parameter Name='Pin name saved' Value='CONV5_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV5_DIR' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:15:2' />
						<Parameter Name='Pin name saved' Value='CONV5_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='7' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV5_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:14:2' />
						<Parameter Name='Pin name saved' Value='CONV5_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='8' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV5_INSNS' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:9:0' />
						<Parameter Name='Pin name saved' Value='CONV5_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='9' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Upstack' >
					<Devices >
						<Device Device_Name='2Conveyor-5' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='10' />
						<Parameter Category='Task Description' Name='Task description' Value='Upstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='CONV5: Purification Plate' />
					<Parameter Name='Plate type' Value='96 Purification' />
					<Parameter Name='Simultaneous plates' Value='1' />
					<Parameter Name='Plates have lids' Value='0' />
					<Parameter Name='Plates enter the system sealed' Value='0' />
					<Parameter Name='Use single instance of plate' Value='0' />
					<Parameter Name='Automatically update labware' Value='0' />
					<Parameter Name='Enable timed release' Value='0' />
					<Parameter Name='Release time' Value='30' />
					<Parameter Name='Auto managed counterweight' Value='0' />
					<Parameter Name='Barcode filename' Value='No Selection' />
					<Parameter Name='Has header' Value='' />
					<Parameter Name='Barcode or header South' Value='No Selection' />
					<Parameter Name='Barcode or header West' Value='No Selection' />
					<Parameter Name='Barcode or header North' Value='No Selection' />
					<Parameter Name='Barcode or header East' Value='No Selection' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV6_DIR' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:17:2' />
						<Parameter Name='Pin name saved' Value='CONV6_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV6_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:16:2' />
						<Parameter Name='Pin name saved' Value='CONV6_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='6' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV6_INSNS' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:11:0' />
						<Parameter Name='Pin name saved' Value='CONV6_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Downstack' >
					<Devices >
						<Device Device_Name='2Conveyor-6_Racks' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Downstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Free empty stackers' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Place Plate' >
					<Devices >
						<Device Device_Name='2Hotel-1' Location_Name='Slot 6' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings />
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Place Plate' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Device to use' Value='2Hotel-1' />
						<Parameter Category='' Name='Location to use' Value='Slot 6' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV6_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:16:2' />
						<Parameter Name='Pin name saved' Value='CONV6_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV6_DIR' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:17:2' />
						<Parameter Name='Pin name saved' Value='CONV6_DIR' />
						<Parameter Category='Task Description' Name='Task number' Value='7' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV6_ENA' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:16:2' />
						<Parameter Name='Pin name saved' Value='CONV6_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='8' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Wait For Input' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Input name' Value='CONV6_INSNS' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for' Value='Exactly' />
						<Parameter Category='' Name='Min or exact value' Value='0' />
						<Parameter Category='' Name='Max value' Value='9999' />
						<Parameter Category='' Name='Timeout' Value='0' />
						<Parameter Category='' Name='On timeout' Value='Continue processing' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:11:0' />
						<Parameter Name='Pin name saved' Value='CONV6_INSNS' />
						<Parameter Category='Task Description' Name='Task number' Value='9' />
						<Parameter Category='Task Description' Name='Task description' Value='Wait For Input' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Upstack' >
					<Devices >
						<Device Device_Name='2Conveyor-6_Racks' Location_Name='Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='10' />
						<Parameter Category='Task Description' Name='Task description' Value='Upstack' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
						<Parameter Category='' Name='Parameter 1' Value='' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='CONV6: Sample Rack' />
					<Parameter Name='Plate type' Value='96 1.4ml Micronic Tube Rack External Thread WITH Caps' />
					<Parameter Name='Simultaneous plates' Value='1' />
					<Parameter Name='Plates have lids' Value='0' />
					<Parameter Name='Plates enter the system sealed' Value='0' />
					<Parameter Name='Use single instance of plate' Value='0' />
					<Parameter Name='Automatically update labware' Value='0' />
					<Parameter Name='Enable timed release' Value='0' />
					<Parameter Name='Release time' Value='30' />
					<Parameter Name='Auto managed counterweight' Value='0' />
					<Parameter Name='Barcode filename' Value='No Selection' />
					<Parameter Name='Has header' Value='' />
					<Parameter Name='Barcode or header South' Value='No Selection' />
					<Parameter Name='Barcode or header West' Value='No Selection' />
					<Parameter Name='Barcode or header North' Value='No Selection' />
					<Parameter Name='Barcode or header East' Value='No Selection' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
		</Main_Processes>
		<Cleanup_Processes >
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV1_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:6:2' />
						<Parameter Name='Pin name saved' Value='CONV1_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV2_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:8:2' />
						<Parameter Name='Pin name saved' Value='CONV2_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV3_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:10:2' />
						<Parameter Name='Pin name saved' Value='CONV3_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV4_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:12:2' />
						<Parameter Name='Pin name saved' Value='CONV4_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV5_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:14:2' />
						<Parameter Name='Pin name saved' Value='CONV5_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='DC24V_PS' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='CONV6_ENA' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:16:2' />
						<Parameter Name='Pin name saved' Value='CONV6_ENA' />
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='turn OFF conveyors' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
		</Cleanup_Processes>
	</Processes>
	<MeasurementTypes >
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='Hotel 1 High Alarm OK' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='1' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='Hotel 1 Low Alarm OK' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='1' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='Hotel 2 High Alarm OK' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='1' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='Hotel 2 Low Alarm OK' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='1' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='5V Input 5' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='1' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='5V Input 6' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='1' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='5V Analog Input 1' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='5' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='5V Analog Input 2' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='5' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='24V Analog Input 1' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='24' />
		<MeasurementType CriticalTime='5' Device_Name='1ACU-1' LogAction='High Low' LowerLimit='0' MeasurementName='24V Analog Input 2' Object_Type='Agilent ACU' PauseAction='High Low' PollFrequency='1' Unit='' UpperLimit='24' />
	</MeasurementTypes>
</Velocity11>