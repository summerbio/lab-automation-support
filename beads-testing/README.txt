This protocol is used to simulate a day of dispensing beads in production. To set it up, do the following:
- open protocol in VW
- in the "define variables" task in the startup protocol, change the javascript as necessary
- make sure imageSavePath is an actual folder
- make sure the unifyCameraInspection is correct (verify in C:\sb-biocel-settings\Rigpa)
- run the protocol and follow instructions!